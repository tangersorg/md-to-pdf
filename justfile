template := "./templates/custom_markdown.tex"
from_args := "markdown+latex_macros+backtick_code_blocks+pipe_tables+table_captions"
filters := "--filter pandoc-latex-color \
--filter ./filters/target/debug/pandoc-alert-box \
--filter ./filters/target/debug/pandoc-page-headers \
--filter ./filters/target/debug/pandoc-code-blocks \
--filter ./filters/target/debug/pandoc-tables \
--filter ./filters/target/debug/pandoc-check-logo"

build_pdf IN="test" OUT="test": build-filters
  pandoc {{IN}}.md --from {{from_args}} -o {{OUT}}.pdf --pdf-engine=xelatex --template={{template}} -f markdown-implicit_figures {{filters}}

build_tex IN="test" OUT="test": build-filters
  pandoc {{IN}}.md --from {{from_args}} --to=latex -o {{OUT}}.tex --template={{template}} -f markdown-implicit_figures {{filters}}

pdf OUT="test":
  firefox {{OUT}}.pdf

build-filters:
  cd filters && cargo build

convert_img IN:
  cat {{IN}}.b64 | cut -d ',' -f 2 | base64 -d > {{IN}}.pdf

small: build-filters
  pandoc test_small.md --from {{from_args}} -o small.pdf --pdf-engine=xelatex --template={{template}} -f markdown-implicit_figures {{filters}}
